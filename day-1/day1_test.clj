(ns day-1.core-test
  (:require [clojure.test :refer :all]
            [day-1.core :refer :all]))

(deftest a-test
  (testing "Day 1 input"
    (is (= (sum-weights "input") 3305041))))
