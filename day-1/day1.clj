(ns day-1.core)

(defn fuel [mass]
  (- (int (/ mass 3)) 2)
)

(defn sum_weights [file_name]
    (with-open [rdr (clojure.java.io/reader file_name)]
    (reduce + 0 (map (comp fuel read-string) (line-seq rdr)))
))

(sum_weights "input")